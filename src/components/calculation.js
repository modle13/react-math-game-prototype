import { useState } from 'react';

import applegreen from '../static/img/apple-green.png';
import applered from '../static/img/apple-red.png';

export default function Calculation(props) {

  const baseClassNames = "";
  const shakeClassNames = "shake buttonFail";
  const successClassNames = "buttonSuccess";
  const [buttonClassNames, updateButtonClassNames] = useState(baseClassNames);
  const [solved, setSolved] = useState(false);

  const processClick = () => {
    if (props.left + props.collectedApples === props.right) {
      setSolved(true);
      setButtonSuccess();
      setTimeout(() => updateConditions(), 1000);
    } else {
      shakeButton();
    }
  }

  function shakeButton() {
    updateButtonClassNames(shakeClassNames);
    // resets shake animation class
    setTimeout(() => updateButtonClassNames(baseClassNames), 1000);
  }

  function setButtonSuccess() {
    updateButtonClassNames(successClassNames);
    // resets shake animation class
    setTimeout(() => updateButtonClassNames(baseClassNames), 1000);
  }

  function updateConditions() {
    props.processSolved();
    setSolved(false);
  }

  function applesRender(count, image) {
    return <div style={{fontSize: '1vh'}}>{Array(count).fill(1).map( e =>
      <img src={image} key={new Date().getTime() * Math.random()}/>
    )}
    </div>
  }

  return (
    <div className="calculation">
      <EquationSide
        applesRender={applesRender}
        count={props.left}
        image={applegreen}
        content={props.left + ' +'}
      />
      <CollectedButton
        applesRender={applesRender}
        collectedApples={props.collectedApples}
        setCollectedApples={props.setCollectedApples}
        setSolved={setSolved}
        extraClassNames={buttonClassNames}
      />
      <EquationSide
        applesRender={applesRender}
        count={props.right}
        image={applered}
        content={'= ' + props.right}
      />
      <CheckButton solved={solved} processClick={processClick} />
    </div>
  );
}

const EquationSide = ({applesRender, count, image, content}) => {
  return (
    <div className="equationSide">
      <div className="equationPart">
        {applesRender(count, image)}
        <div>{content}</div>
      </div>
    </div>
  )
};

const CollectedButton = ({applesRender, collectedApples, setCollectedApples, setSolved, extraClassNames}) => {
  return (
    <div className="equationSide">
      <div className="equationPart">
        {applesRender(collectedApples, applegreen)}
        <button
          type="button"
          style={{marginTop: '-10px'}}
          className={'equationPart ' + extraClassNames}
          onClick={() => {setCollectedApples(Math.max(collectedApples - 1, 0)); setSolved(false)}}
        >{collectedApples}
        </button>
      </div>
    </div>
  )
};

const CheckButton = ({solved, processClick}) => {
  return (
    <div>
      <button type="button" className="checkButton" onClick={processClick}>
        {solved ? 'you win!' : 'check'}
      </button>
    </div>
  );
};
