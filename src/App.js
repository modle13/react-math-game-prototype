import './App.css';

import Game from './components/game';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        Adding Apples
      </header>
      <Game />
    </div>
  );
}

export default App;
