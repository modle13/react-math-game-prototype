# Adding Apples

A simple addition game written with the React framework.
 
## Hosted demo

https://adding-apples.matthewodle.com/

## Running locally

```
npm install
npm start
```

This should load a browser window and navigate to http://localhost:3000 automatically.

## Design Notes

### High-level Requirements

- uses functional components
- uses react hooks
- has animations
- contains gameplay elements related to elementary-level math

### High-level Design

This will be a simple complete-the-equation game.

The second number in an addition formula will be missing.

This missing number will be modified by collecting apples.

Apples can be collected by shaking the apple tree, then clicking on the fallen apple.

A button will check the state of the entered number.

### Animations

- upon spawning, apples will rotate and fall until they reach the base of the tree
- when clicked, the tree will shake
- if the check button is clicked when the equation is incorrect, the button representing the collected apples will shake and turn red
